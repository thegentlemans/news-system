-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time:  7 авг 2019 в 21:37
-- Версия на сървъра: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `borisov`
--

-- --------------------------------------------------------

--
-- Структура на таблица `coverages`
--

CREATE TABLE `coverages` (
  `id` int(17) NOT NULL,
  `cover` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура на таблица `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `description` varchar(300) NOT NULL,
  `content` text NOT NULL,
  `time` datetime NOT NULL,
  `autor` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `news`
--

INSERT INTO `news` (`id`, `image`, `description`, `content`, `time`, `autor`, `publisher`) VALUES
(1, 'kjkjk', '', '', '0000-00-00 00:00:00', '', ''),
(2, '2', '', '', '2019-08-07 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` text NOT NULL,
  `phone` varchar(16) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `image`, `phone`) VALUES
(1, 'borisov', '$2y$10$s2kpo0mKkDIOvyCgiLmuV.cugNfphyWJRXCDPEaHxWXVGiyHcNlt6', 'bamchou@abv.bg', 'ivence.jpg', '0998000'),
(5, 'isoss', '$2y$10$3Gd8ZaV/JzCWMXBzFA3FSOPB8RyZrK5fL7PagqhTrXuQ5EYqpJIze', 'sss@dance.bg', '', ''),
(4, 'borisov2', '$2y$10$99hUq1ElIYdTv8THIVHgJOa2CVAam/VWxsgsV9hAyNtBNn4t.46hi', 'seso@seso.bg', '', '000'),
(6, 'maraba', '$2y$10$mB0F0nhhu/r50ihNfYsd6.RmDSiOwqwHYyxz7He3wp9U2IbJS/onS', 'maraba@mail.com', 'ivence.jpg', ''),
(7, 'test', '$2y$10$8fTA728uA7rykVC26biEB.Jf0OLUsVE4AdjXc4.eJJkaBsezejL/W', 'test@mail.com', '24_JURCHEN-Phone-Case-For-Huawei-P30-Pro-Cover-For-Huawei-P30-Case-Cartoon-TPU-Silicone-Soft.jpg', '828282828');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `coverages`
--
ALTER TABLE `coverages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `coverages`
--
ALTER TABLE `coverages`
  MODIFY `id` int(17) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
