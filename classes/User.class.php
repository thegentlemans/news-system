<?php
/****************
* - News Engine 2019
* - Author : Ivaylo Borisov
* - Designer : Krasimir Mladenov
****************/

class User
{
	private $username, $email, $phone, $db, $id;
/////////////////////////////////////////CONNECT TO DB FUNCTION I.Borisov/////////////////////////////////////

	public function __construct($dbh)
	{
		$this->db = $dbh;
	}
/////////////////////////////////////////CONNECT TO DB FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////UPDATE SETTINGS FUNCTION I.Borisov/////////////////////////////////////


	public function updateSettings($p, $id , $username,$first_name,$last_name,$aboutme)
	{
        $_SESSION['user_info']['phone'] = $p;
        $_SESSION['user_info']['first_name'] = $first_name;
        $_SESSION['user_info']['last_name'] = $last_name;
        $_SESSION['user_info']['aboutme'] = $aboutme;
		$q = $this->db->pdo->prepare("update users set phone = ?, fname = ?, lname = ?, bio = ? where username = ?");
		$q->execute([$p,$first_name,$last_name,$aboutme, $username]);
		$this->phone = $p;
        $this->fname = $first_name;
        $this->lname = $last_name;
        $this->bio = $aboutme;
        session_start();
	}

/////////////////////////////////////////UPDATE SETTINGS FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////GET PUBLIC PROFILE INFO FUNCTION I.Borisov/////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
    public function getfollow($u)
	{
		if($this->usernameExists($u))
		{
			    $q = $this->db->pdo->prepare("select id, password, phone,image,bio,cover,fname,lname from users where username = ?");
                $q->execute([$u]);
                $row = $q->fetch(PDO::FETCH_ASSOC);
				$this->username = $u;
				$this->id = $row['id'];
				$this->phone = $row['phone'];
                $this->image = $row['image'];
                $this->bio = $row['bio'];
                $this->cover = $row['cover'];
                $this->fname = $row['fname'];
                $this->lname = $row['lname'];


           return array(
			"username" => $this->username,
			"id" => $this->id,
			"phone" => $this->phone,
            "image" => $this->image,
            "aboutme" => $this->bio,
            "cover" => $this->cover,
            "first_name" => $this->fname,
            "last_name" => $this->lname
		);

		} else {
			return false;
		}
	}
////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////GET PUBLIC PROFILE INFO FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////UPLOAD PROFILE IMAGE FUNCTION I.Borisov/////////////////////////////////////

public function uploadimage($img,$id,$username)
{
$_SESSION['user_info']['image'] = $_FILES["fileToUpload"];
if (!file_exists("uploads/profile/$username")) {   
mkdir("uploads/profile/$username", 0700);
}

$target_dir = "uploads/profile/$username/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image

    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
       // echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      //  echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        $img = basename( $_FILES["fileToUpload"]["name"]);
        echo $img;
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
		$q = $this->db->pdo->prepare("update users set image = ? where username = ?");
		$q->execute([$img, $username]);
		$this->image = $img;
       session_start();
//unset($_SESSION['user_info']['username']);

	}

/////////////////////////////////////////UPLOAD PROFILE IMAGE FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////UPLOAD COVER FUNCTION I.Borisov/////////////////////////////////////
public function uploadcover($cover,$id,$username)
{
$_SESSION['user_info']['cover'] = $_FILES["fileToUpload2"];
if (!file_exists("uploads/profile/$username/cover/")) {   
mkdir("uploads/profile/$username/cover/", 0700);
}

$target_dir = "uploads/profile/$username/cover/";
$target_file = $target_dir . basename($_FILES["fileToUpload2"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image

    $check = getimagesize($_FILES["fileToUpload2"]["tmp_name"]);
    if($check !== false) {
       // echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }

// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["fileToUpload2"]["size"] > 50000000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload2"]["tmp_name"], $target_file)) {
      //  echo "The file ". basename( $_FILES["fileToUpload2"]["name"]). " has been uploaded.";
        $cover = basename( $_FILES["fileToUpload2"]["name"]);
        echo $cover;
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
		$q = $this->db->pdo->prepare("update users set cover = ? where username = ?");
		$q->execute([$cover, $username]);
		$this->cover = $cover;
       session_start();
//unset($_SESSION['user_info']['username']);

}
/////////////////////////////////////////UPLOAD COVER FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////GET LOGGED USER INFO FUNCTION I.Borisov/////////////////////////////////////

	//string
	public function getUserInfo()
	{
		return array(
			"username" => $this->username,
			"id" => $this->id,
			"phone" => $this->phone,
            "image" => $this->image,
            "aboutme" => $this->bio,
            "cover" => $this->cover,
            "first_name" => $this->fname,
            "last_name" => $this->lname
		);
           session_start();
	}
/////////////////////////////////////////GET LOGGED USER INFO FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////GET FUNCTIONS I.Borisov/////////////////////////////////////

	public function getPhone()
	{
		return $this->phone;
	}

	public function getFirstname()
	{
		return $this->first_name;
	}

	public function getLastname()
	{
		return $this->last_name;
	}
    
	public function getAboutme()
	{
		return $this->aboutme;
	}

    public function getImage()
	{
		return $this->image;
	}
    
     public function getCover()
	{
		return $this->cover;
	}
/////////////////////////////////////////GET FUNCTIONS I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////LOGIN FUNCTION I.Borisov/////////////////////////////////////

	//bool
	public function login($u, $p)
	{
		if($this->usernameExists($u))
		{
			$q = $this->db->pdo->prepare("select id, password, phone,image,bio,cover,fname,lname from users where username = ?");
			$q->execute([$u]);
			$row = $q->fetch(PDO::FETCH_ASSOC);
			$pass = $row['password'];

			if(password_verify($_POST['user_pass'], $pass))
			{
				$this->username = $u;
				$this->id = $row['id'];
				$this->phone = $row['phone'];
                $this->image = $row['image'];
                $this->bio = $row['bio'];
                $this->cover = $row['cover'];
                $this->fname = $row['fname'];
                $this->lname = $row['lname'];

				return true;
			}
		} else {
			return false;
		}
	}
/////////////////////////////////////////LOGIN FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////REGISTER FUNCTION I.Borisov/////////////////////////////////////
	//bool
	public function create($u, $e, $p,$phone)
	{
		$pass = password_hash($p, PASSWORD_DEFAULT);
		$u = filter_var($u, FILTER_SANITIZE_STRING);

		if(filter_var($e, FILTER_VALIDATE_EMAIL))
		{
			$email = $e;
		} else {
			die("invalid email address");
		}



		if(!$this->usernameExists($u) && !$this->emailExists($e))
		{
            
            $time = date("Y-m-d h:i:sa");
			$q = $this->db->pdo->prepare("insert into users (username,phone, password, email,signup_date) values(?,?,?,?,?)");
			$q->execute([$u,$phone, $pass, $e,$time]);
            
            echo "Your account is ready <a href=\"home?page=login\">Login</a>";
		} else {
			die("username or email already exists, <a href=\"home?page=register\">try again</a>");
		}
	}

/////////////////////////////////////////REGISTER FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/




//////////////////////////////////////////INSERT NEWS TO DATABASE ////////////////////////////////////////
public function insertnews()
{
      $urlArticles = file_get_contents('https://newsapi.org/v2/top-headlines?country=us&apiKey=e04726f98e6c405899df6e5b490ecf0d');
      $urlArticlesArray = json_decode($urlArticles, true);

       $articles = $urlArticlesArray['articles'];

       for($i = 0; $i < count($articles); $i++) {
           
           $sites = $urlArticlesArray['articles'][$i]; 
           $title = $sites['title'];
           $image = $sites['urlToImage'];
           $description = $sites['description'];
           $author = $sites['author'];
           $time = $sites['publishedAt'];
           $content = $sites['content'];
           if(empty($content)){
               $content = "No Content";
       }
           $q = $this->db->pdo->prepare("insert into news (title,image,description, publisher, time,content) values(?,?,?,?,?,?)");
           $q->execute([$title,$image,$description, $author, $time,$content]);

         }
}
//////////////////////////////////////////INSERT NEWS TO DATABASE ////////////////////////////////////////

/***********************************************************************************************************/


//////////////////////////////////////////INSERT SOURCES TO DATABASE ////////////////////////////////////////
public function InsertSources()
{
      $urlArticles = file_get_contents('https://newsapi.org/v2/sources?&apiKey=e04726f98e6c405899df6e5b490ecf0d');
      $urlArticlesArray = json_decode($urlArticles, true);

       $articles = $urlArticlesArray['sources'];

       for($i = 0; $i < count($articles); $i++) {
           
           $sites = $urlArticlesArray['sources'][$i]; 
           $name = $sites['id'];
           $fullname = $sites['name'];
           $description = $sites['description'];
           $url = $sites['url'];
           $category = $sites['category'];
           $language = $sites['language'];
           $country = $sites['country'];

			$q = $this->db->pdo->prepare("insert into sources (name,fullname, description,url,category,language,country) values(?,?,?,?,?,?,?)");
			$q->execute([$name,$fullname, $description, $url,$category,$language,$country]);
 echo "Inserted<br>";
         }
}
//////////////////////////////////////////INSERT SOURCES TO DATABASE ////////////////////////////////////////

/***********************************************************************************************************/





/////////////////////////////////////////CHECK USERNAME FUNCTION I.Borisov/////////////////////////////////////

	//bool
	public function usernameExists($u)
	{
		$q = $this->db->pdo->prepare("select username from users where username = ?");
		$q->execute([$u]);
		return $q->fetch(PDO::FETCH_ASSOC);
	}
/////////////////////////////////////////CHECK USERNAME FUNCTION I.Borisov/////////////////////////////////////

/***********************************************************************************************************/

/////////////////////////////////////////CHECK EMAIL FUNCTION I.Borisov/////////////////////////////////////

	//bool
	public function emailExists($e)
	{
		$q = $this->db->pdo->prepare("select email from users where email = ?");
		$q->execute([$e]);
		return $q->fetch(PDO::FETCH_ASSOC);
	}


/////////////////////////////////////////CHECK EMAIL FUNCTION I.Borisov/////////////////////////////////////









//////////////////////////////////////////FOLLOW DATABASE ////////////////////////////////////////
public function followme($id,$followid)
{


		if(!$this->checkfollow($followid,$id) == 0)
		{
checkfollow($followid,$id);
echo "<br>insert";
            
        }
else
{
echo "<br>Not insert";
}
//$q = $this->db->pdo->prepare("insert into sources (name,fullname, description,url,category,language,country) values(?,?,?,?,?,?,?)");
//$q->execute([$name,$fullname, $description, $url,$category,$language,$country]);
        echo "<hr>";
        echo $id;
                echo "<hr>";
        echo $followid;
}
//////////////////////////////////////////INSERT SOURCES TO DATABASE ////////////////////////////////////////

/***********************************************************************************************************/



//////////////////////////////////////////UNFOLLOW DATABASE ////////////////////////////////////////
public function unfollowme()
{
$count = checkfollow($id,$followid);

$q = $this->db->pdo->prepare("insert into sources (name,fullname, description,url,category,language,country) values(?,?,?,?,?,?,?)");
$q->execute([$name,$fullname, $description, $url,$category,$language,$country]);
echo "Inserted<br>";
}
//////////////////////////////////////////INSERT SOURCES TO DATABASE ////////////////////////////////////////

/***********************************************************************************************************/





//////////////////////////////////////////CHECK FOLLOW DATABASE ////////////////////////////////////////
 function checkfollow($followid,$id)
{
			    $q = $this->db->pdo->prepare("select count(*) as counter from following where user_id= ? and follower_id= ? ");
                $q->execute([$followid,$id]);
                return $q->fetch(PDO::FETCH_ASSOC);
    
}
//////////////////////////////////////////INSERT SOURCES TO DATABASE ////////////////////////////////////////

/***********************************************************************************************************/



}
?>