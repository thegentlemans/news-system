<?php namespace example;

/**
 * To run this example:
 * $ php read_channel.php CHANNEL-ID KEY-ID KEY-SECRET
 *
 * Which should produce output like this: 201
 * {
 *   "data":{
 *     "createdAt":"2016-06-13T00:25:43Z",
 *     "modifiedAt":"2016-06-13T23:30:05Z",
 *     "id":"4cf91021-f696-4706-81bd-2f799f6fb40a",
 *     "type":"channel",
 *     "shareUrl":"https://apple.news/TESx4XIzZMi1kL2Q29Q2ghh",
 *   ...
 * }
 */

error_reporting(E_ERROR | E_PARSE);

class PublisherAPI
{
    var $currentAction;
    var $channelId;
    var $keyId;
    var $keySecret;
    var $url;

    /**
     * Prepares the request by creating a signature to validate
     * the client, prepares the headers and then sends the
     * request to the Apple News Server
     *
     * @param String $method HTTP request "POST/GET"
     * @param String $url URL
     *
     * @return array of the server response
     */
    function sendRequest($method, $url)
    {
        $date = gmdate("Y-m-d\TH:i:s\Z");
        $canonicalRequest = $method . $url . $date;
        $signature = $this->createSignature($canonicalRequest);
        $authorization = "HHMAC; key=" . $this->keyId . "; signature=" . $signature . "; date=" . $date;

        $headers = array();
        array_push($headers, "Authorization: " . $authorization);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return array(
            status_code => $statusCode,
            response => $result
        );
    }

    /**
     * Prepares the URL and then calls sendRequest to
     * communicate with the news server to get channel
     * data
     *
     * @return array of the server response
     */
    function readChannel()
    {
        $method = "GET";
        $url = $this->url . $this->channelId;

        return $this->sendRequest($method, $url);
    }

    /**
     * Creates an encrypted string to validate user creditials
     * communicating with the Apple News Server
     *
     * @param String $canonicalRequest A string to encrypt
     *
     * @return string base64 string of 256 bit encrypted key hash
     */
    function createSignature($canonicalRequest)
    {
        $keyBytes = base64_decode($this->keySecret);
        $hashed = hash_hmac('sha256', $canonicalRequest, $keyBytes, true);
        return base64_encode($hashed);
    }

    /**
     * Main
     *
     * @param Array $inputArguements A string to encrypt
     *
     * @param Array $inputArguements Array of command line arguments
     */
    function main()
    {
        switch ($this->currentAction) {
            case "readChannel":
                $response = $this->readChannel();
                break;

            default:
                $response = array(
                    status_code => 400,
                    response => '{"errors":[{"code":"UNKNOWN_COMMAND"}]}"'
                );
                break;

        }

        return $response;
    }
}

if ($argc < 4 ) {
    exit('no or missing arguments');
}

$publisherAPI = new PublisherAPI();

$publisherAPI->channelId = $argv[1];
$publisherAPI->keyId = $argv[2];
$publisherAPI->keySecret = $argv[3];
$publisherAPI->url = "https://news-api.apple.com/channels/";
$publisherAPI->currentAction = "readChannel";

if (!empty($argv[4])) {
    $publisherAPI->currentAction = "createArticle";
}

$response = $publisherAPI->main();

print_r($response);
echo "\n";
?>
