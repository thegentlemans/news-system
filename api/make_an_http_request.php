<?php namespace example;

 /**
 * To run this example:
 * $ php make_an_http_request.php CHANNEL-ID
 *
 * Which should return an error, because we haven't signed the request yet.
 * 401
 * {
 *   "errors":[
 *     {
 *       "code":"UNAUTHORIZED"
 *     }
 *   ]
 * }
 */

error_reporting(E_ERROR | E_PARSE);

class PublisherAPI
{
    var $currentAction;
    var $channelId;

    /**
     * Prepares the URL and then attempts to
     * directly call the Apple News Server
     * without proper credentials
     *
     * @return string HTTP Response Error Header
     */
    function readChannel()
    {
        $url = $this->url . $this->channelId;
        $remote = fopen($url, 'r');

        if (!$remote) {
            return "Error: " . $http_response_header[0];
        } else {
            return "Success";
        }
    }

    /**
     * Main
     *
     * @param Array $inputArguements A string to encrypt
     *
     * @param Array $inputArguements Array of command line arguments
     */
    function main()
    {
        switch ($this->currentAction) {
            case "readChannel":
                $response = $this->readChannel();
                break;

            default:
                $response = array(
                    status_code => 400,
                    response => '{"errors":[{"code":"UNKNOWN_COMMAND"}]}"'
                );
                break;

        }

        return $response;
    }
}

if ($argc < 2 ) {
    exit('no or missing arguments');
}

$publisherAPI = new PublisherAPI();

$publisherAPI->channelId = $argv[1];
$publisherAPI->url = "https://news-api.apple.com/channels/";
$publisherAPI->currentAction = "readChannel";

$response = $publisherAPI->main();

print_r($response);
echo "\n";
?>
