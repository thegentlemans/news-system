<?php namespace example;

/**
 * To run this example:
 * $ php create_article.php CHANNEL-ID KEY-ID KEY-SECRET ARTICLE-DIRECTORY
 *
 * Which should produce output like this:
 * 201
 * {
 *   "data":{
 *     "createdAt":"2016-06-13T00:25:43Z",
 *     "modifiedAt":"2016-06-13T23:30:05Z",
 *     "id":"4cf91021-f696-4706-81bd-2f799f6fb40a",
 *     "type":"article",
 *     "shareUrl":"https://apple.news/AhEMS4XIzZigh2Qx2921kLQ",
 *   ...
 * }
 */
error_reporting(E_ERROR | E_PARSE);

class PublisherAPI
{
    var $currentAction;
    var $channelId;
    var $keyId;
    var $keySecret;
    var $url;
    var $articleDirectory;
    var $boundary;

    /**
     * Builds the body to be sent to Apple News Server.
     * Goes through all of the files in the article directory
     *
     * @return Array Array of contentType and Body of content.
     */
    function buildArticleBody()
    {
        $this->boundary = substr(md5(microtime()), rand(0, 26), 64);
        $contentType = "multipart/form-data; boundary=" . $this->boundary;
        $filenames = scandir($this->articleDirectory);
        $filenames = array_filter($filenames, array($this, 'filterFiles'));
        $parts = array_map(array($this, 'buildMimePart'), $filenames);

        if (empty($parts)) {
            exit($this->articleDirectory . " doesn't appear to be a valid article bundle!");
        }

        $body = implode("\r\n", $parts);
        $body .= sprintf("\r\n--%s--", $this->boundary);

        return array(body => $body, contentType => $contentType);
    }

    /**
     * Builds the POSTDATA of a file
     *
     * @param String $filename Filename
     *
     * @return string POSTDATA of contentType and the file contents
     */
    function buildMimePart($filename)
    {
        $fullFilename = $this->articleDirectory . "/" . $filename;
        $contentType = $this->guessContentType($filename);
        if ($contentType === null) {
            return "";
        }
        $part = sprintf("--%s\r\n", $this->boundary);
        $part .= sprintf("Content-Type: %s\r\n", $contentType);
        $part .= sprintf("Content-Disposition: form-data; filename=%s; size=%d\r\n\r\n", $filename, filesize($fullFilename));
        $part .= file_get_contents($fullFilename);

        return $part;
    }

    /**
     * Determines the mimetype of the input file
     *
     * @param String $filename Filename
     *
     * @return string filename that does not start with ., otherwise NULL
     */
    function filterFiles($filname)
    {
        return substr($filname, 0, 1) !== ".";
    }

    /**
     * Determines the mimetype of the input file
     *
     * @param String $filename Filename
     *
     * @return string of mimetype
     */
    function guessContentType($filename)
    {
        $filenameComponents = explode(".", $filename);
        $extension = $filenameComponents[1];
        if ($filename == "article.json") {
            return "application/json";
        } elseif ($extension == "jpg" || $extension == "jpeg") {
            return "image/jpeg";
        } elseif ($extension == "gif") {
            return "image/gif";
        } elseif ($extension == "png") {
            return "image/png";
        }

        return null;
    }

    /**
     * Prepares the request by creating the article body
     * and assets to be sent to Apple News Server
     *
     * @return array of the server response
     */
    function createArticle()
    {
        $method = "POST";
        $url = $this->url . $this->channelId . "/articles";
        $article = $this->buildArticleBody();

        return $this->sendRequest($method, $url, $article);
    }

    /**
     * Prepares the request by creating a signature to validate
     * the client, prepares the headers and then sends the
     * request to the Apple News Server
     *
     * @param String $method HTTP request "POST/GET"
     * @param String $url URL
     * @param Array $article Array of Body and Content Type
     *              or NULL if requesting channel data
     *
     * @return array of the server response
     */
    function sendRequest($method, $url, $article = null)
    {
        $body = null;
        $contentType = null;

        if (!empty($article)) {
            $body = $article["body"];
            $contentType = $article["contentType"];
        }

        $date = gmdate("Y-m-d\TH:i:s\Z");
        $canonicalRequest = $method . $url . $date;
        if ($body !== null) {
            $canonicalRequest .= $contentType . $body;
        }

        $signature = $this->createSignature($canonicalRequest);
        $authorization = "HHMAC; key=" . $this->keyId . "; signature=" . $signature . "; date=" . $date;

        $headers = array();
        array_push($headers, "Authorization: " . $authorization);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($body !== null) {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
            array_push($headers, "Content-Type: " . $contentType);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        return array(
            status_code => $statusCode,
            result => $result
        );
    }

    /**
     * Prepares the URL and then calls sendRequest to
     * communicate with the news server to get channel
     * data
     *
     * @return array of the server response
     */
    function readChannel()
    {
        $method = "GET";
        $url = $this->url . $this->channelId;

        return $this->sendRequest($method, $url);
    }

    /**
     * Creates an encrypted string to validate user creditials
     * communicating with the Apple News Server
     *
     * @param String $canonicalRequest A string to encrypt
     *
     * @return string base64 string of 256 bit encrypted key hash
     */
    function createSignature($canonicalRequest)
    {
        $keyBytes = base64_decode($this->keySecret);
        $hashed = hash_hmac('sha256', $canonicalRequest, $keyBytes, true);

        return base64_encode($hashed);
    }

    /**
     * Main
     *
     * @param Array $inputArguements A string to encrypt
     *
     * @param Array $inputArguements Array of command line arguments
     */
    function main()
    {
        switch ($this->currentAction) {
            case "createArticle":
                $response = $this->createArticle();
                break;

            case "readChannel":
                $response = $this->readChannel();
                break;
            default:
                $response = array(
                    status_code => 400,
                    response => '{"errors":[{"code":"UNKNOWN_COMMAND"}]}"'
                );
                break;

        }

        return $response;
    }
}

if ($argc < 4 ) {
    exit('no or missing arguments');
}

$publisherAPI = new PublisherAPI();

$publisherAPI->channelId = $argv[1];
$publisherAPI->keyId = $argv[2];
$publisherAPI->keySecret = $argv[3];
$publisherAPI->articleDirectory = $argv[4];
$publisherAPI->url = "https://news-api.apple.com/channels/";
$publisherAPI->currentAction = "readChannel";

if (!empty($argv[4])) {
    $publisherAPI->currentAction = "createArticle";
}

$response = $publisherAPI->main();

print_r($response);
echo "\n";
?>
