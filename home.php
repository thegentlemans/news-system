<?php
error_reporting(1);
ini_set('display_errors', 1);

//start the session
session_start();

//include the smarty library
include("vendor/autoload.php");

//include the database library
include("classes/Db.class.php");

//include the user library
include("classes/User.class.php");

//create a new database object
$db = new Db("inc/config");

//create a new smarty object
$smarty = new Smarty;
$smarty->caching = 2; // lifetime is per cache


//set the template directory
$smarty->setTemplateDir("templates/default");
//get the page variable from HTTP GET
if(@$_GET['page'])
{
	$page = filter_var($_GET['page'], FILTER_SANITIZE_STRING);
}
$logged = $_SESSION['logged_in'];
$id_kur = $_SESSION['user_info']['id'];
$username = $_SESSION['user_info']['username'];
$phone = $_SESSION['user_info']['phone'];
$image2 = $_SESSION['user_info']['image'];
$image = $image2['name'];
if (strlen($image) <= 2){
    $image = $_SESSION['user_info']['image'];
}else
{
$image2 = $_SESSION['user_info']['image'];
$image = $image2['name'];
}
$cover2 = $_SESSION['user_info']['cover'];
$cover = $cover2['name'];
if (strlen($cover) <= 2){
    $cover = $_SESSION['user_info']['cover'];
}else
{
$cover2 = $_SESSION['user_info']['cover'];
$cover = $cover2['name'];
}
$first_name = $_SESSION['user_info']['first_name'];
$last_name = $_SESSION['user_info']['last_name'];
$aboutme = $_SESSION['user_info']['aboutme'];
//what page should we display?
switch($page)
{
	default:
      $smarty->assign("page", 'home');    
      $smarty->assign("image", $image);
      $smarty->assign("cover", $cover);
      $smarty->assign("aboutme", $aboutme);
      $smarty->assign("first_name", $first_name);
      $smarty->assign("last_name", $last_name);
      $smarty->assign("id", $_SESSION['user_info']['id']);
      $smarty->assign("username", $_SESSION['user_info']['username']);
      $smarty->assign("phone", $phone);       
      //show the index template file
      $smarty->display("layout/header.tpl");
      $urlArticles = file_get_contents('https://newsapi.org/v2/top-headlines?country=us&apiKey=e04726f98e6c405899df6e5b490ecf0d&category=business');
      $urlArticlesArray = json_decode($urlArticles, true);
      $articles = $urlArticlesArray['articles'];
      $smarty->assign("articles", $articles); 
      $smarty->display("index.tpl");
      $smarty->display("layout/footer.tpl");
	break;

   case "uploadimage":
        $smarty->assign("page", $page);
        $smarty->assign("image", $image);
        $smarty->assign("cover", $cover);
        $smarty->assign("aboutme", $aboutme);
        $smarty->assign("first_name", $first_name);
        $smarty->assign("last_name", $last_name);
        $smarty->assign("id", $_SESSION['user_info']['id']);
        $smarty->assign("username", $_SESSION['user_info']['username']);
        $smarty->assign("phone", $phone); 
		$smarty->display("layout/header.tpl");
	    $smarty->display("profile/uploadimage.tpl");
		$smarty->display("layout/footer.tpl");
	break;    

	case "login":
        $smarty->assign("image", $image);
        $smarty->assign("cover", $cover);
        $smarty->assign("id", $_SESSION['user_info']['id']);
        $smarty->assign("username", $_SESSION['user_info']['username']);
        $smarty->assign("phone", $phone);  
		$smarty->display("layout/header.tpl");
		$smarty->display("pages/login.tpl");
		$smarty->display("layout/footer.tpl");
	break;
        
        
	case "coverages":
        $smarty->assign("image", $image);
        $smarty->assign("cover", $cover);
        $smarty->assign("aboutme", $aboutme);
        $smarty->assign("first_name", $first_name);
        $smarty->assign("last_name", $last_name);
        $smarty->assign("id", $_SESSION['user_info']['id']);
        $smarty->assign("username", $_SESSION['user_info']['username']);
        $smarty->assign("phone", $phone); 
		$smarty->display("layout/header.tpl");
		$smarty->display("pages/coverages.tpl");
		$smarty->display("layout/footer.tpl");
	break;

	case "register":
		if(!$_SESSION['logged_in'])
		{
           $smarty->assign("image", $image);
            $smarty->assign("cover", $cover);
            $smarty->assign("id", $_SESSION['user_info']['id']);
            $smarty->assign("username", $_SESSION['user_info']['username']);
            $smarty->assign("phone", $phone); 
			$smarty->display("layout/header.tpl");
			$smarty->display("pages/register.tpl");
			$smarty->display("layout/footer.tpl");
		}
	break;

	case "settings":
        $smarty->assign("image", $image);
        $smarty->assign("cover", $cover);
        $smarty->assign("aboutme", $aboutme);
        $smarty->assign("first_name", $first_name);
        $smarty->assign("last_name", $last_name);
        $smarty->assign("id", $_SESSION['user_info']['id']);
        $smarty->assign("username", $_SESSION['user_info']['username']);
        $smarty->assign("phone", $phone);  
		$smarty->display("layout/header.tpl");
		$smarty->assign("phone", $_SESSION['user_info']['phone']);
		$smarty->display("profile/settings.tpl");
		$smarty->display("layout/footer.tpl");
	break;
//SECOND UPDATE NOT ASSIGN TPL FILES
	case "update_settings":
		$id = $_SESSION['user_info']['id'];
		$p = $_POST['phone_num'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $aboutme = $_POST['aboutme'];
		$user = new User(new Db("inc/config"));
		$user->updateSettings($p, $id,$username,$first_name,$last_name,$aboutme);
		header('Location: home?page=settings');

	break;

	case "logout":
		session_destroy();
		header('Location: home');
	break;

	case "user_login":
		$user = new User(new Db("inc/config"));
		if($user->login($_POST['user_name'], $_POST['password']))
		{
			$_SESSION['logged_in'] = true;
			$_SESSION['user_info'] = $user->getUserInfo();
		}
		header('Location: home');
	break;

	case "user_register":
		$u = $_POST['user_name'];
		$p = $_POST['user_pass'];
		$e = $_POST['user_email'];
        $phone = $_POST['user_phone'];
		$user = new User(new Db("inc/config"));
		$user->create($u, $e, $p,$phone);
		//header('Location: home');
	break;
    

        
   case "upload_image":
		var_dump($_POST);
        $id = $_SESSION['user_info']['id'];
		$img = $_POST['fileToUpload'];
		$user = new User(new Db("inc/config"));
		$user->uploadimage($img,$id,$username);
	    //echo $username;
		header('Location: home?page=uploadimage');
        
        
        case "upload_sources":
        $id = $_SESSION['user_info']['id'];
		$user = new User(new Db("inc/config"));
		$user->InsertSources();
	    //echo $username;
     break;      
       case "upload_cover":
        $id = $_SESSION['user_info']['id'];
		$cover = $_POST['fileToUpload2'];
		$user = new User(new Db("inc/config"));
		$user->uploadcover($cover,$id,$username);
		header('Location: home?page=uploadimage');
     break; 
        
    case "refresh_news":
      $user = new User(new Db("inc/config"));
      $user->insertnews();
      header('Location: home');
    break;    
        
}

?>