{if $smarty.session.logged_in}
<div class="form-group">
  <label for="fileToUpload">Profile Image:</label>
<form action="home?page=upload_image" method="POST" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload" value="{$image}">
    <input type="submit" value="Upload Image" name="submit">
</form>
</div>

<hr>

<div class="form-group">
<label for="fileToUpload2">Cover Image:</label>
<form action="home?page=upload_cover" method="POST" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload2" id="fileToUpload2" value="{$cover}">
    <input type="submit" value="Upload Image" name="submit">
</form>
</div>
{else}
<p>You are not logged in</p>
{/if}