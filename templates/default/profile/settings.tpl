<h1>Account settings</h1>

<div class="card">
	<div class="card-header">
		Account
	</div>
	<div class="card-body">
		{if $smarty.session.logged_in}

			<form method="post" action="home?page=update_settings">
                
               <div class="form-group">
					<label for="phone_num">First Name</label>
					<input type="text" id="first_name" name="first_name" class="form-control" value="{$first_name}" placeholder="{$first_name}">
				</div>

               <div class="form-group">
					<label for="phone_num">Last Name</label>
					<input type="text" id="last_name" name="last_name" class="form-control" value="{$last_name}" placeholder="{$last_name}">
				</div>
                
                <div class="form-group">
					<label for="phone_num">About Me (Max 500 symbols)*</label>
                    <textarea type="text" id="aboutme" name="aboutme" class="form-control" value="{$aboutme}" placeholder="{$aboutme}">{$aboutme}</textarea>
				</div>
                
				<div class="form-group">
					<label for="phone_num">Phone Number</label>
					<input type="text" id="phone_num" name="phone_num" class="form-control" value="{$phone}" placeholder="{$phone}">
				</div>
                
                
                

				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		{else}
			<p>You are not logged in</p>
		{/if}

	</div>
</div>
