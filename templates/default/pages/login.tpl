{* <div class="card">
	<div class="card-header">
		Login
	</div>
	<div class="card-body"> *}
		{if !$smarty.session.logged_in}
			<form method="post" action="home?page=user_login">
				<div class="form-group">
					<label for="user_name">Username</label>
					<input type="text" id="user_name" name="user_name" class="form-control" aria-describedby="emailHelp" placeholder="Enter username">
					<small id="emailHelp" class="form-text text-muted">We'll never share your username with anyone else.</small>
				</div>

				<div class="form-group">
					<label for="user_pass">Password</label>
					<input type="password" id="user_pass" name="user_pass" class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>

				<button class="btn btn-outline-secondary btn-lg" data-dismiss="modal" aria-hidden="true">Cancel</button>
				<button type="submit" class="btn btn-primary btn-lg float-right">Login</button>
			</form>
		{else}
			<p>You are not logged in</p>
		{/if}
{* 
	</div>
</div> *}
