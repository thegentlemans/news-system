<div class="card">
	<div class="card-header">
		Register
	</div>
	<div class="card-body">
		{if !$smarty.session.logged_in}
			<form method="post" action="home?page=user_register">
				<div class="form-group">
					<label for="user_name">Username</label>
					<input type="text" id="user_name" name="user_name" required class="form-control" aria-describedby="emailHelp" placeholder="Enter username">
					<small id="emailHelp" class="form-text text-muted">We'll never share your username with anyone else.</small>
				</div>

				<div class="form-group">
					<label for="user_pass">Password</label>
					<input type="password" id="user_pass" name="user_pass" required class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>

				<div class="form-group">
					<label for="user_email">Email</label>
					<input type="email" id="user_email" name="user_email" required class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>

				<div class="form-group">
					<label for="user_phone">Phone Number</label>
					<input type="text" id="user_phone" name="user_phone" required class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>


				<button type="submit" class="btn btn-primary">Register</button>
			</form>
		{else}
			<p>You are not logged in</p>
		{/if}

	</div>
</div>