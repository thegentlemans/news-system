<h3>Coverages</h3>

<div class="card-deck">
	<div class="card">
		<div class="card-body">
			<div class="d-flex flex-row justify-content-between align-items-center align-content-between flex-wrap" style="margin-bottom: 20px;">
				<img src="https://www.w3schools.com/howto/img_avatar.png" alt="avatar" class="rounded" style="width: 50px; height: 50px;">
				<div class="" style="flex: 0 0 80%;">
					<p class="intro" style="margin: 0">Coverage curated by</p>
					<p name="author" style="margin: 0">Jim Kennedy</p>
				</div>
			</div>

			<h5 class="text-muted font-weight-bold">Apr 23 - June 3</h5>
			<h3 class="card-title">What is the core of Palestinian conflict?</h3>
		</div>

		<img class="card-img-bottom" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22253%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20253%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c91b6ae8b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c91b6ae8b%22%3E%3Crect%20width%3D%22253%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2293.25%22%20y%3D%2296%22%3E253x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
		
		<p class="card-text d-flex flex-row justify-content-between align-items-center">
			<small class="text-muted">Last updated 3 mins ago</small>
			
			<button type="button" class="btn btn-primary btn-sm">
				<i class="fas fa-plus"></i>
				Add
			</button>
		</p>
	</div>

	<div class="card">
		<div class="card-body">
			<div class="d-flex flex-row justify-content-between align-items-center align-content-between flex-wrap" style="margin-bottom: 20px;">
				<img src="https://www.w3schools.com/howto/img_avatar.png" alt="avatar" class="rounded" style="width: 50px; height: 50px;">
				<div class="" style="flex: 0 0 80%;">
					<p class="intro" style="margin: 0">Coverage curated by</p>
					<p name="author" style="margin: 0">Jim Kennedy</p>
				</div>
			</div>

			<h5 class="text-muted font-weight-bold">Apr 23 - June 3</h5>
			<h3 class="card-title">What is the core of Palestinian conflict?</h3>
		</div>

		<img class="card-img-bottom" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22253%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20253%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c91b6ae8b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c91b6ae8b%22%3E%3Crect%20width%3D%22253%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2293.25%22%20y%3D%2296%22%3E253x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
		
		<p class="card-text d-flex flex-row justify-content-between align-items-center">
			<small class="text-muted">Last updated 3 mins ago</small>
			
			<button type="button" class="btn btn-primary btn-sm">
				<i class="fas fa-plus"></i>
				Add
			</button>
		</p>
	</div>

	<div class="card">
		<div class="card-body">
			<div class="d-flex flex-row justify-content-between align-items-center align-content-between flex-wrap" style="margin-bottom: 20px;">
				<img src="https://www.w3schools.com/howto/img_avatar.png" alt="avatar" class="rounded" style="width: 50px; height: 50px;">
				<div class="" style="flex: 0 0 80%;">
					<p class="intro" style="margin: 0">Coverage curated by</p>
					<p name="author" style="margin: 0">Jim Kennedy</p>
				</div>
			</div>

			<h5 class="text-muted font-weight-bold">Apr 23 - June 3</h5>
			<h3 class="card-title">What is the core of Palestinian conflict?</h3>
		</div>

		<img class="card-img-bottom" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22253%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20253%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c91b6ae8b%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A13pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c91b6ae8b%22%3E%3Crect%20width%3D%22253%22%20height%3D%22180%22%20fill%3D%22%23777%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%2293.25%22%20y%3D%2296%22%3E253x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" alt="Card image cap">
		
		<p class="card-text d-flex flex-row justify-content-between align-items-center">
			<small class="text-muted">Last updated 3 mins ago</small>
			
			<button type="button" class="btn btn-primary btn-sm">
				<i class="fas fa-plus"></i>
				Add
			</button>
		</p>
	</div>
</div>