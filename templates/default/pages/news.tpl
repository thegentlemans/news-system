<h1>News Feed</h1>

{* User it's logged in *}
{if $smarty.session.logged_in}
    {* {$articles|print_r} *}
    {foreach from=$articles item=article}
        <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="{$article['urlToImage']}" alt="{$article['title']}" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            {$article['title']}
                            {if !empty($article['author'])} author: {$article['author']}{/if} 
                            <span class="badge badge-pill badge-info">{$article['publishedAt']}
                        </h4>
                        <p class="card-text">
                            {$article['description']}
                        </p>
                        <a href="{$article['url']}" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
            {* <div class="card-footer w-100 text-muted">
                Footer stating cats are CUTE little animals
            </div> *}
        </div>

    {/foreach}

{* User without account *}
{else}
    <p>Please Login to view news !</p>
{/if}