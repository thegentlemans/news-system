<nav class="navbar navbar-expand-sm news-feed-account-bar">
    {if $smarty.session.logged_in}
        <div class="btn-group">
            <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-cog"></i>
            </button>
            
            <ul class="dropdown-menu">
                <li class="dropdown-header">
                    <a class="navbar-brand" href="profile?username={$username}">
                        <img src="uploads/profile/{$username}/{$image}" alt="avatar-{$username}" style="
                            width: 60px;
                            margin: -15px 0px -15px 0px;
                            clip-path: circle(30px at center);
                        ">
                        {$username}
                    </a>
                </li>
                <li class="dropdown-header">Settings</li>
                <li>
                    <a class="dropdown-item" href="home?page=settings">
                        <i class="far fa-address-card"></i> Settings
                    </a>
                </li>
                <li>
                    <a class="dropdown-item" href="home?page=uploadimage">
                        <i class="fas fa-camera"></i> Upload Images
                    </a>
                </li>

                <li class="dropdown-header"><hr></li>
                <li>
                    <a class="dropdown-item" href="home?page=logout">
                        <i class="fas fa-sign-out-alt"></i> Logout
                    </a>
                </li>
            </ul>
        </div>

    {else}
        <!-- Register / Login -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="home?page=register">
        			<i class="fas fa-file-signature"></i>
                    Register 
                </a>
            </li>
            <li class="nav-item">
                {* <a class="nav-link" href="home?page=login">
        			<i class="fas fa-sign-in-alt"></i>
                    Login
                </a> *}

                <a href="#loginModal" role="button" class="nav-link" data-toggle="modal">
        			<i class="fas fa-sign-in-alt"></i>
                    Login
                </a>
            </li>
        </ul>
    {/if}
</nav>

<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3>Login</h3>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                {include file="pages/login.tpl"}
            </div>
        </div>
    </div>
</div>