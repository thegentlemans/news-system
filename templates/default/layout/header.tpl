<!doctype html>
<html lang="en">

<head>
	<title>Home</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="assets/js/libs/fontAwesome-ff8bec5533.js"></script>

	<link rel="stylesheet" href="assets/style/css/libs/bootstrap.css">
	<link rel="stylesheet" href="assets/style/css/general.min.css">
</head>

<body>
{include file="layout/sidebars.tpl"}

{* Global container of pages *}
<main class="main-container container-fluid">
	{include file="layout/account-navbar.tpl"}