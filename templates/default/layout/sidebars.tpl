
{* Sidebar fixed menu *}
<div class="sidenav-left">
	<a href="home"><img src="web/images/logo.png" class="logo"></a>

    {* Menu which is available when you are logged in *}
	{if $smarty.session.logged_in}
        <div class="global-menu">
            <a href="home" data-toggle="tooltip" data-placement="right" title="News Feed">
                <i class="fas fa-newspaper"></i>
            </a>
            <a href="home?page=coverages" data-toggle="tooltip" data-placement="right" title="Coverages">
                <i class="fas fa-columns"></i>
            </a>
            <a href="home" data-toggle="tooltip" data-placement="right" title="Images">
                <i class="fas fa-image"></i>
            </a>
            <a href="home" data-toggle="tooltip" data-placement="right" title="Comments">
                <i class="fas fa-comment-alt"></i>
            </a>
        </div>
	{/if}
</div>

<div class="sidenav-right">
	<ul>
        {for $foo=1 to 5}
            <li>
                <img src="web/source-example.png" class="img-source" alt="">
                <span class="remove-source">X</span>
            </li>
        {/for}

        <li data-toggle="modal" data-target="#myModal">
            <span class="add-source">+</span>
        </li>
    </ul>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>