<?php
error_reporting(1);
ini_set('display_errors', 1);

//start the session
session_start();

//include the smarty library
include("vendor/autoload.php");

//include the database library
include("classes/Db.class.php");

//include the user library
include("classes/User.class.php");


//create a new database object
$db = new Db("inc/config");

//create a new smarty object
$smarty = new Smarty;

//set the template directory
$smarty->setTemplateDir("templates/default");
$smarty->setCacheDir('cache');

//get the page variable from HTTP GET
if(@$_GET['page'])
{
	$page = filter_var($_GET['page'], FILTER_SANITIZE_STRING);
}

$logged = $_SESSION['logged_in'];
$id_kur = $_SESSION['user_info']['id'];
$username = $_SESSION['user_info']['username'];
$phone = $_SESSION['user_info']['phone'];
$image2 = $_SESSION['user_info']['image'];
$image = $image2['name'];

if (strlen($image) <= 2){
    $image = $_SESSION['user_info']['image'];
}
else {
    $image2 = $_SESSION['user_info']['image'];
    $image = $image2['name'];
}

$cover2 = $_SESSION['user_info']['cover'];
$cover = $cover2['name'];

if (strlen($cover) <= 2) {
    $cover = $_SESSION['user_info']['cover'];
}
else {
    $cover2 = $_SESSION['user_info']['cover'];
    $cover = $cover2['name'];
}

$first_name = $_SESSION['user_info']['first_name'];
$last_name = $_SESSION['user_info']['last_name'];
$aboutme = $_SESSION['user_info']['aboutme'];



// print_r($_SESSION);

//what page should we display?
switch($page)
{
	default:
        //show the index template file
        $smarty->assign("image", $image);
        $smarty->assign("cover", $cover);
        $smarty->assign("aboutme", $aboutme);
        $smarty->assign("first_name", $first_name);
        $smarty->assign("last_name", $last_name);
        $smarty->assign("id", $_SESSION['user_info']['id']);
        $smarty->assign("username", $_SESSION['user_info']['username']);
        $smarty->assign("phone", $phone); 
          
        $smarty->display("layout/header.tpl");
        $smarty->display("profile/profile.tpl");
        $smarty->display("layout/footer.tpl");
	    break;
}