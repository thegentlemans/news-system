<?php
/* Smarty version 3.1.33, created on 2019-08-13 00:55:49
  from 'C:\xampp\htdocs\news-system\templates\default\settings.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d51ee754b2983_30467045',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f23b699c271284878a370b0ff8606f27f37b46e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\news-system\\templates\\default\\settings.tpl',
      1 => 1565305953,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 3600,
),true)) {
function content_5d51ee754b2983_30467045 (Smarty_Internal_Template $_smarty_tpl) {
?><h1>Account settings</h1>

<div class="card">
	<div class="card-header">
		Account
	</div>
	<div class="card-body">
		
			<form method="post" action="home?page=update_settings">
                
               <div class="form-group">
					<label for="phone_num">First Name</label>
					<input type="text" id="first_name" name="first_name" class="form-control" value="Ivaylo" placeholder="Ivaylo">
				</div>

               <div class="form-group">
					<label for="phone_num">Last Name</label>
					<input type="text" id="last_name" name="last_name" class="form-control" value="Borisov" placeholder="Borisov">
				</div>
                
                <div class="form-group">
					<label for="phone_num">About Me (Max 500 symbols)*</label>
                    <textarea type="text" id="aboutme" name="aboutme" class="form-control" value="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book." placeholder="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</textarea>
				</div>
                
				<div class="form-group">
					<label for="phone_num">Phone Number</label>
					<input type="text" id="phone_num" name="phone_num" class="form-control" value="+10878550460" placeholder="+10878550460">
				</div>
                
                
                

				<button type="submit" class="btn btn-primary">Update</button>
			</form>
		
	</div>
</div>
<?php }
}
