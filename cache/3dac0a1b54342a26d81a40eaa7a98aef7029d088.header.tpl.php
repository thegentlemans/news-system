<?php
/* Smarty version 3.1.33, created on 2019-08-13 00:55:49
  from 'C:\xampp\htdocs\news-system\templates\default\header.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d51ee753b5c29_84717613',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '63f1c82203c1969ff61e33995b0bee2d0d1564b7' => 
    array (
      0 => 'C:\\xampp\\htdocs\\news-system\\templates\\default\\header.tpl',
      1 => 1565299003,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 3600,
),true)) {
function content_5d51ee753b5c29_84717613 (Smarty_Internal_Template $_smarty_tpl) {
?><!doctype html>
<html lang="en">

<head>
	<title>Home</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<script src="assets/js/libs/fontAwesome-ff8bec5533.js"></script>

	<link rel="stylesheet" href="assets/css/libs/bootstrap.css">
	<link rel="stylesheet" href="assets/css/general.css">
</head>

<body>

<div class="sidenav">
	<a href="home"><img src="web/images/logo.png" class="logo"></a>

			<a href="home?page=settings">
			<i class="far fa-address-card"></i>
		</a>
		<a href="home?page=uploadimage">
			<i class="fas fa-camera"></i>
		</a>
        <a href="home?page=coverages">
			<i class="fa fa-heart-o"></i>
		</a>
		<a href="home?page=logout">
			<i class="fas fa-sign-out-alt"></i>
		</a>
	</div>

<main class="main-container container-fluid">
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<!-- Brand/logo -->
		<a class="navbar-brand" href="profile?username=borisov">
			<img src="uploads/profile/borisov/ivence.jpg" alt="avatar-borisov" style="width:40px;">
		</a>
		
		<!-- Links -->
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="#">Link 1</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Link 2</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="#">Link 3</a>
			</li>
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
					Dropdown link
				</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="#">Link 1</a>
					<a class="dropdown-item" href="#">Link 2</a>
					<a class="dropdown-item" href="#">Link 3</a>
				</div>
			</li>
		</ul>
	</nav><?php }
}
