<?php
/* Smarty version 3.1.33, created on 2019-08-15 16:03:46
  from 'C:\xampp\htdocs\news-system\templates\default\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d556642b76fe8_41467723',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e7b7919c187e86de8963a7a3038621f362a46892' => 
    array (
      0 => 'C:\\xampp\\htdocs\\news-system\\templates\\default\\index.tpl',
      1 => 1565724967,
      2 => 'file',
    ),
    '1a7ec1729b8a438c528149453e4d73965834a869' => 
    array (
      0 => 'C:\\xampp\\htdocs\\news-system\\templates\\default\\pages\\news.tpl',
      1 => 1565724967,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 3600,
),true)) {
function content_5d556642b76fe8_41467723 (Smarty_Internal_Template $_smarty_tpl) {
?><h1>News Feed</h1>

                <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://i-invdn-com.akamaized.net/trkd-images/LYNXNPEF7E15H_L.jpg" alt="U.S. retail sales surge in July in boost to economy - Investing.com" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            U.S. retail sales surge in July in boost to economy - Investing.com
                             
                            <span class="badge badge-pill badge-info">2019-08-15T12:41:00Z
                        </h4>
                        <p class="card-text">
                            U.S. retail sales surge in July in boost to economy
                        </p>
                        <a href="https://www.investing.com/news/economic-indicators/us-retail-sales-surge-in-july-in-boost-to-economy-1956084" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://image.cnbcfm.com/api/v1/image/106013903-1562851440361rts2jijy.jpg?v=1562851475" alt="US productivity grew a healthy 2.3% rate in the 2nd quarter - CNBC" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            US productivity grew a healthy 2.3% rate in the 2nd quarter - CNBC
                             author: The Associated Press 
                            <span class="badge badge-pill badge-info">2019-08-15T12:33:58Z
                        </h4>
                        <p class="card-text">
                            U.S. productivity increased at a decent pace in the second quarter, a trend that could lead to higher wages if it continues.
                        </p>
                        <a href="https://www.cnbc.com/2019/08/15/us-productivity-labor-costs---q2-2019.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://image.cnbcfm.com/api/v1/image/106080711-1565869452091gettyimages-97208273.jpeg?v=1565869492" alt="GE shares drop after Madoff whistleblower Harry Markopolos calls it a 'bigger fraud than Enron' - CNBC" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            GE shares drop after Madoff whistleblower Harry Markopolos calls it a 'bigger fraud than Enron' - CNBC
                             author: John Melloy 
                            <span class="badge badge-pill badge-info">2019-08-15T12:15:48Z
                        </h4>
                        <p class="card-text">
                            GE says in a statement that Madoff whistleblower's allegations of fraud are "entirely false and misleading."
                        </p>
                        <a href="https://www.cnbc.com/2019/08/15/ge-shares-drop-after-madoff-whistleblower-harry-markopolos-raises-red-flags-on-its-accounting.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://image.cnbcfm.com/api/v1/image/105767278-1551371385895gettyimages-1063544288.jpeg?v=1565870838" alt="JC Penney reports mixed second-quarter results as sales decline at a faster-than-expected pace - CNBC" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            JC Penney reports mixed second-quarter results as sales decline at a faster-than-expected pace - CNBC
                             author: Jasmine Wu 
                            <span class="badge badge-pill badge-info">2019-08-15T12:00:27Z
                        </h4>
                        <p class="card-text">
                            J.C. Penney on Thursday reported mixed second-quarter results that showed its sales are continuing to erode.
                        </p>
                        <a href="https://www.cnbc.com/2019/08/15/jc-penney-reports-q2-2019-earnings.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://static3.seekingalpha.com/assets/og_image_192-59bfd51c9fe6af025b2f9f96c807e46f8e2f06c5ae787b15bf1423e6c676d4db.png" alt="Pot stocks in the red after Canopy Growth's FQ1 miss - Seeking Alpha" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Pot stocks in the red after Canopy Growth's FQ1 miss - Seeking Alpha
                             author: SA Editor Douglas W. House 
                            <span class="badge badge-pill badge-info">2019-08-15T11:34:00Z
                        </h4>
                        <p class="card-text">
                            Cannabis producers are under modest pressure premarket on the heels of Canopy Growth's less-than-expected fiscal Q1 results released after the close yesterday.Tilray has also been weighing on the grou
                        </p>
                        <a href="https://seekingalpha.com/news/3492102-pot-stocks-red-canopy-growths-fq1-miss" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="" alt="Markets in turmoil amid recession fears - CBS This Morning" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Markets in turmoil amid recession fears - CBS This Morning
                             
                            <span class="badge badge-pill badge-info">2019-08-15T11:24:28Z
                        </h4>
                        <p class="card-text">
                            
                        </p>
                        <a href="https://www.youtube.com/watch?v=qcdFzHIc2Z4" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://i2.wp.com/www.cordcuttersnews.com/wp-content/uploads/2018/05/CBS-2.jpg?fit=1333%2C746&ssl=1" alt="What Does CBS & Viacom Merger Mean for PlayStation Vue, Hulu, Sling TV, & Others? - Cord Cutters News, LLC" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            What Does CBS & Viacom Merger Mean for PlayStation Vue, Hulu, Sling TV, & Others? - Cord Cutters News, LLC
                             author: https://www.facebook.com/CordCuttersNews/ 
                            <span class="badge badge-pill badge-info">2019-08-15T11:08:00Z
                        </h4>
                        <p class="card-text">
                            This week, we learned that CBS and Viacom have agreed to merge into a single company called ViacomCBS. This has raised many questions about what this means for services like PlayStation Vue, Sling TV, and Hulu. So, today, we want to address all of the questio…
                        </p>
                        <a href="https://www.cordcuttersnews.com/what-does-cbs-viacom-merger-mean-for-playstation-vue-hulu-sling-tv-others/" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://mms.businesswire.com/media/20190815005317/en/674555/23/AlibabaLogo_%E7%9B%B4%E5%BC%8F%E6%A0%87%E5%87%86%E4%B8%AD%E8%8B%B1%E7%89%88_cropped.jpg" alt="Alibaba Group Announces June Quarter 2019 Results - Business Wire" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Alibaba Group Announces June Quarter 2019 Results - Business Wire
                             
                            <span class="badge badge-pill badge-info">2019-08-15T11:05:00Z
                        </h4>
                        <p class="card-text">
                            Alibaba Group Holding Limited (NYSE: BABA) today announced its financial results for the quarter ended June 30, 2019.
                        </p>
                        <a href="https://www.businesswire.com/news/home/20190815005317/en/Alibaba-Group-Announces-June-Quarter-2019-Results" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://s.yimg.com/uu/api/res/1.2/h400QmBNf25.uD537KKqlA--~B/aD0yNTkyO3c9Mzg3MjtzbT0xO2FwcGlkPXl0YWNoeW9u/https://media-mbst-pub-ue1.s3.amazonaws.com/creatr-images/2019-08/b44e17e0-bf4b-11e9-b9ff-751cffdca706" alt="Walmart beats sales expectations, boosts guidance - Yahoo Finance" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Walmart beats sales expectations, boosts guidance - Yahoo Finance
                             
                            <span class="badge badge-pill badge-info">2019-08-15T11:05:00Z
                        </h4>
                        <p class="card-text">
                            Retail giant Walmart beat expectations on both the top and bottom lines for its second quarter, and the company also raised its profit and sales growth forecast for the year.
                        </p>
                        <a href="https://finance.yahoo.com/news/walmart-earnings-q2-2020-110548616.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://o.aolcdn.com/images/dims?thumbnail=1200%2C630&quality=80&image_uri=https%3A%2F%2Fo.aolcdn.com%2Fimages%2Fdims%3Fresize%3D2000%252C2000%252Cshrink%26image_uri%3Dhttps%253A%252F%252Fs.yimg.com%252Fos%252Fcreatr-uploaded-images%252F2019-08%252Fb2c8a3a0-bf3b-11e9-ade7-006838309eb1%26client%3Da1acac3e1b3290917d92%26signature%3D2786c3d62ae8d80d43a5dad0493f2de4cda5346b&client=amp-blogside-v2&signature=7973a3989e18e016365a9f65188b10c34c734042" alt="The Morning After: The mystery of Virginia's TV Man - Engadget" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            The Morning After: The mystery of Virginia's TV Man - Engadget
                             author: Mat Smith 
                            <span class="badge badge-pill badge-info">2019-08-15T11:03:22Z
                        </h4>
                        <p class="card-text">
                            Sleep with a HDMI cable under your pillow.
                        </p>
                        <a href="https://www.engadget.com/2019/08/15/the-morning-after-the-mystery-of-virginias-tv-man/" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://cdn.cnn.com/cnnnext/dam/assets/190815105313-tencent-file-restricted-super-tease.jpg" alt="Tencent profits bounce back. But there's trouble ahead - CNN" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Tencent profits bounce back. But there's trouble ahead - CNN
                             author: Laura He, CNN Business 
                            <span class="badge badge-pill badge-info">2019-08-15T10:57:00Z
                        </h4>
                        <p class="card-text">
                            Tencent's profit jumped 35% in the second quarter thanks to a sharp turnaround in its video game business. But more challenges are ahead.
                        </p>
                        <a href="https://www.cnn.com/2019/08/15/investing/tencent-china-economy/index.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://media.graytvinc.com/images/amazon+mgn4.jpg" alt="Amazon will donate unsold merchandise instead of trashing it - KCRG" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Amazon will donate unsold merchandise instead of trashing it - KCRG
                             author: KCRG 
                            <span class="badge badge-pill badge-info">2019-08-15T10:52:58Z
                        </h4>
                        <p class="card-text">
                            The company said it will start donating the items that did not sell or that have been returned.
                        </p>
                        <a href="https://www.kcrg.com/content/news/Amazon-will-donate-unsold-merchandise-instead-of-trashing-it-543328651.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://a57.foxnews.com/static.foxbusiness.com/foxbusiness.com/content/uploads/2019/08/0/0/MandS190815.png?ve=1&tl=1" alt="Movers & Shakers: Aug. 15, 2019 - Fox Business" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Movers & Shakers: Aug. 15, 2019 - Fox Business
                             author: FOXBusiness 
                            <span class="badge badge-pill badge-info">2019-08-15T10:38:44Z
                        </h4>
                        <p class="card-text">
                            Stories moving the markets and shaking up the world.
                        </p>
                        <a href="https://www.foxbusiness.com/markets/movers-shakers-aug-15-2019" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://i-invdn-com.akamaized.net/news/LYNXMPEB2A073_L.jpg" alt="Top 5 Things to Know in the Market on Thursday - Investing.com" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Top 5 Things to Know in the Market on Thursday - Investing.com
                             
                            <span class="badge badge-pill badge-info">2019-08-15T10:35:00Z
                        </h4>
                        <p class="card-text">
                            Top 5 Things to Know in the Market on Thursday
                        </p>
                        <a href="https://www.investing.com/news/economy/top-5-things-to-know-in-the-market-on-thursday-1955885" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://www.10tv.com/sites/default/files/styles/article_image/public/2019/08/15/four_loko_seltzer.jpg?itok=Qiw5qLIq" alt="Four Loko teases new seltzer with nearly triple the alcohol content of its competitors - 10TV" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Four Loko teases new seltzer with nearly triple the alcohol content of its competitors - 10TV
                             author: https://www.facebook.com/CBSNews 
                            <span class="badge badge-pill badge-info">2019-08-15T10:34:50Z
                        </h4>
                        <p class="card-text">
                            Four Loko, an alcoholic beverage line once popular with millennials, appears to be jumping on the hard seltzer trend in an extreme way. In a series of tweets, Four Loko teased a seltzer boasting a much higher alcohol-by-volume amount than its main competitors.
                        </p>
                        <a href="https://www.10tv.com/article/four-loko-teases-new-seltzer-nearly-triple-alcohol-content-its-competitors-2019-aug" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://image.cnbcfm.com/api/v1/image/106046769-1564435585812gettyimages-1063575224.jpeg?v=1565797386" alt="Walmart shares jump 5% on earnings beat, raised outlook - CNBC" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Walmart shares jump 5% on earnings beat, raised outlook - CNBC
                             author: Lauren Thomas 
                            <span class="badge badge-pill badge-info">2019-08-15T10:18:22Z
                        </h4>
                        <p class="card-text">
                            Walmart on Thursday reported earnings that topped expectations and raised its outlook for the full year, building on the momentum in its core U.S. business and investments into grocery.
                        </p>
                        <a href="https://www.cnbc.com/2019/08/15/walmart-reports-q2-fiscal-2020-earnings.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://cdn.cnn.com/cnnnext/dam/assets/190814223723-china-port-0806-super-tease.jpg" alt="Asian markets roiled by recession fears, but Shanghai and Hong Kong recover - CNN" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Asian markets roiled by recession fears, but Shanghai and Hong Kong recover - CNN
                             author: Laura He, CNN Business 
                            <span class="badge badge-pill badge-info">2019-08-15T09:31:00Z
                        </h4>
                        <p class="card-text">
                            Asian stocks were roiled by recession fears Thursday, though markets in Hong Kong and Shanghai recovered their losses by the end of the day.
                        </p>
                        <a href="https://www.cnn.com/2019/08/14/investing/asian-market-latest-bonds-recession/index.html" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://images.barrons.com/im-98607/social" alt="Stocks Plunged After the Yield Curve Inverted. History Says Don’t Worry Yet - Barron's" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Stocks Plunged After the Yield Curve Inverted. History Says Don’t Worry Yet - Barron's
                             author: Nicholas Jasinski 
                            <span class="badge badge-pill badge-info">2019-08-15T09:00:00Z
                        </h4>
                        <p class="card-text">
                            Every single recession since the 1950s was preceded by an inversion of the yield curve, with very few false positives. But there tends to be a delay.
                        </p>
                        <a href="https://www.barrons.com/articles/yield-curve-inversion-stocks-recession-history-51565840376" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="http://s.marketwatch.com/public/resources/MWimages/MW-HP476_maersk_ZG_20190815044257.jpg" alt="Europe stocks inch higher after pounding over economy fears - MarketWatch" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            Europe stocks inch higher after pounding over economy fears - MarketWatch
                             author: Steve Goldstein 
                            <span class="badge badge-pill badge-info">2019-08-15T07:45:00Z
                        </h4>
                        <p class="card-text">
                            A fledgling recovery in European stocks was stamped out on Thursday after China threatened countermeasures to U.S. tariffs.
                        </p>
                        <a href="https://www.marketwatch.com/story/europe-stocks-inch-higher-after-pounding-over-economy-fears-2019-08-15" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

            <div class="card" style="margin-bottom: 35px;">
            <div class="row no-gutters">
                <div class="col-auto">
                    <img src="https://s4.reutersmedia.net/resources_v2/images/rcom-default.png" alt="U.S. yield curve remains inverted for second day - Reuters" class="img-fluid" style="width: 280px;">
                </div>
                <div class="col">
                    <div class="card-block px-2">
                        <h4 class="card-title">
                            U.S. yield curve remains inverted for second day - Reuters
                             author: Reuters Editorial 
                            <span class="badge badge-pill badge-info">2019-08-15T06:46:00Z
                        </h4>
                        <p class="card-text">
                            The U.S. yield curve was inverted for the second straight trading session on Thursday, as investors' concerns that the world's biggest economy could be heading for recession deepened.
                        </p>
                        <a href="https://www.reuters.com/article/us-us-treasuries-yield/u-s-yield-curve-remains-inverted-for-second-day-idUSKCN1V50KM" target="_blank" class="btn btn-primary">See Article</a>
                    </div>
                </div>
            </div>
                    </div>

    
<?php }
}
