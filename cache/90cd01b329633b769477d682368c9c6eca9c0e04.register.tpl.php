<?php
/* Smarty version 3.1.33, created on 2019-08-12 22:10:19
  from 'C:\xampp\htdocs\news-system\templates\default\register.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d51c7abe11793_54667108',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a3e95750cd98126557794d3ae0321dd734634321' => 
    array (
      0 => 'C:\\xampp\\htdocs\\news-system\\templates\\default\\register.tpl',
      1 => 1565295924,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 3600,
),true)) {
function content_5d51c7abe11793_54667108 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="card">
	<div class="card-header">
		Register
	</div>
	<div class="card-body">
					<form method="post" action="home?page=user_register">
				<div class="form-group">
					<label for="user_name">Username</label>
					<input type="text" id="user_name" name="user_name" required class="form-control" aria-describedby="emailHelp" placeholder="Enter username">
					<small id="emailHelp" class="form-text text-muted">We'll never share your username with anyone else.</small>
				</div>

				<div class="form-group">
					<label for="user_pass">Password</label>
					<input type="password" id="user_pass" name="user_pass" required class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>

				<div class="form-group">
					<label for="user_email">Email</label>
					<input type="email" id="user_email" name="user_email" required class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>

				<div class="form-group">
					<label for="user_phone">Phone Number</label>
					<input type="text" id="user_phone" name="user_phone" required class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>


				<button type="submit" class="btn btn-primary">Register</button>
			</form>
		
	</div>
</div><?php }
}
