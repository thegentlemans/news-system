<?php
/* Smarty version 3.1.33, created on 2019-08-12 22:10:21
  from 'C:\xampp\htdocs\news-system\templates\default\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d51c7ad927ff1_03048577',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '803fe111debc9a359e08d8a2fde0a91cb12bf0d0' => 
    array (
      0 => 'C:\\xampp\\htdocs\\news-system\\templates\\default\\login.tpl',
      1 => 1565174361,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 3600,
),true)) {
function content_5d51c7ad927ff1_03048577 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="card">
	<div class="card-header">
		Login
	</div>
	<div class="card-body">
					<form method="post" action="home?page=user_login">
				<div class="form-group">
					<label for="user_name">Username</label>
					<input type="text" id="user_name" name="user_name" class="form-control" aria-describedby="emailHelp" placeholder="Enter username">
					<small id="emailHelp" class="form-text text-muted">We'll never share your username with anyone else.</small>
				</div>

				<div class="form-group">
					<label for="user_pass">Password</label>
					<input type="password" id="user_pass" name="user_pass" class="form-control" aria-describedby="emailHelp" placeholder="*********">
				</div>

				<button type="submit" class="btn btn-primary">Login</button>
			</form>
		
	</div>
</div>
<?php }
}
